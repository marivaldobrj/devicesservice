/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufba.dcc.controller;

import br.ufba.dcc.service.Usuario;
import br.ufba.dcc.service.UsuarioServiceDAO_Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Marivaldo
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {"/login"})
public class LoginController extends HttpServlet{
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/UsuarioServiceDAO/UsuarioServiceDAO.wsdl")
    private UsuarioServiceDAO_Service service;

    private static final long serialVersionUID = 1L;
    
    public LoginController() {
        super();
    }

    @Override
    public void init() throws ServletException {
        System.out.println("LoginController inicializado!");
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = ((HttpServletRequest)request).getSession();
        Usuario usuario = (Usuario)session.getAttribute("usuario");
        if(usuario == null) // nao esta logado
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        else // ja esta logado
            response.sendRedirect(request.getContextPath() + "/");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = request.getSession(); //obtem a sessao do usuario, caso exista
        
        Usuario usuario = null;
        
        String login_form = request.getParameter("login"); // Pega o Login vindo do formulario
        String senha_form = request.getParameter("senha"); 
        
        try {
            usuario = authorizeUser(login_form, senha_form);
        }
        catch ( Exception e ){
 
        }
        
        //se nao encontrou usuario no banco, redireciona para a pagina de erro!
        if ( usuario == null ) {
            session.setAttribute("notification_type", "danger");
            session.setAttribute("notification_message", "Dados incorretos!");
            session.invalidate();
            request.getRequestDispatcher("/login.jsp" ).forward(request, response);
        }
        else{
            //se o dao retornar um usuario, coloca o mesmo na sessao
            session.setAttribute("usuario", usuario);
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    private Usuario authorizeUser(java.lang.String arg0, java.lang.String arg1) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.authorizeUser(arg0, arg1);
    }
    
    
}
