/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufba.dcc.controller;

import br.ufba.dcc.service.Usuario;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Marivaldo
 */
public class AuthorizationController implements Filter{
 
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        HttpSession session = ((HttpServletRequest)request).getSession();
        Usuario usuario = (Usuario)session.getAttribute("usuario");
        
        if(usuario==null){
            session.setAttribute("notification_type", "danger");
            session.setAttribute("notification_message", "Você precisa estar logado para visualizar esta página!");
            String contextPath = ((HttpServletRequest)request).getContextPath();
            ((HttpServletResponse)response).sendRedirect(contextPath+"/login.jsp");
        }else{
            chain.doFilter(request, response);        
        }
    }

    @Override
    public void destroy() {
    }
    
}
