/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufba.dcc.controller;

import br.ufba.dcc.service.Dispositivo;
import br.ufba.dcc.service.Usuario;
import br.ufba.dcc.service.UsuarioServiceDAO_Service;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Marivaldo
 */
@WebServlet(loadOnStartup = 3, urlPatterns = {"/device/listdevices", "/device/adddevice"})
public class DispositivoController extends HttpServlet{
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/UsuarioServiceDAO/UsuarioServiceDAO.wsdl")
    private UsuarioServiceDAO_Service service;

    private static final long serialVersionUID = 1L;
    
    public DispositivoController(){
        super();
    }

    @Override
    public void init() throws ServletException {
       System.out.println("DispositivoController inicializado!");
    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getServletPath();
        
        switch (url) {
            case "/device/adddevice":
                request.getRequestDispatcher("/device/adddevice.jsp").forward(request, response);
                break;
            case "/device/listdevices":
                String usuarioId = request.getParameter("usuario_id");
                if(usuarioId != null){ // exibe dispositivos de um usuario
                    request.setAttribute("dispositivos", listDevices(Integer.parseInt(usuarioId)));
                    request.setAttribute("usuario", getUserById(Integer.parseInt(usuarioId)));
                }else{ // exibe dispositivos de todos os usuarios
                    request.setAttribute("dispositivos", listAllDevices());
                }

                request.getRequestDispatcher("/device/listdevices.jsp").forward(request, response);
                break;
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = ((HttpServletRequest)request).getSession();
        Usuario usuario = (Usuario)session.getAttribute("usuario");
        
        Dispositivo dispositivo = new Dispositivo();
        int usuario_id = usuario.getId();
        
        dispositivo.setDescricao(request.getParameter("descricao"));
        
        addDevice(new javax.xml.ws.Holder<Dispositivo>(dispositivo), usuario_id);
        
        response.sendRedirect(request.getContextPath()+"/device/listdevices?usuario_id="+usuario_id);
    }

    private void addDevice(javax.xml.ws.Holder<br.ufba.dcc.service.Dispositivo> dispositivo, int arg1) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        port.addDevice(dispositivo, arg1);
    }

    private java.util.List<br.ufba.dcc.service.Dispositivo> listDevices(int arg0) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.listDevices(arg0);
    }

    private java.util.List<br.ufba.dcc.service.Dispositivo> listAllDevices() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.listAllDevices();
    }

    private Usuario getUserById(int arg0) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.getUserById(arg0);
    }

}
