package br.ufba.dcc.controller;
import br.ufba.dcc.service.Usuario;
import br.ufba.dcc.service.UsuarioServiceDAO_Service;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceRef;

@WebServlet(loadOnStartup = 2, urlPatterns = {"/user/adduser", "/user/listuser", "/user/edituser"})
public class UsuarioController extends HttpServlet {
    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/UsuarioServiceDAO/UsuarioServiceDAO.wsdl")
    private UsuarioServiceDAO_Service service;
    
    private static final long serialVersionUID = 1L;

    public UsuarioController() {
        super();
    }

    @Override
    public void init() throws ServletException {
        System.out.println("UsuarioController inicializado!");
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getServletPath();
        
        switch (url) {
            case "/user/adduser":
                HttpSession session = ((HttpServletRequest)request).getSession();
                Usuario usuario = (Usuario)session.getAttribute("usuario");
                if(usuario == null) // nao esta logado
                    request.getRequestDispatcher("/user/adduser.jsp").forward(request, response);
                else // ja esta logado
                    response.sendRedirect(request.getContextPath() + "/");
                break;  
            case "/user/listuser":
                request.setAttribute("usuarios", listUser());
                request.getRequestDispatcher("/user/listuser.jsp").forward(request, response);
                break;  
            case "/user/edituser":
                request.getRequestDispatcher("/user/edituser.jsp").forward(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        Usuario usuario;
        
        switch (action) {
            case "add":
                usuario = new Usuario();
                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario = addUser(usuario);
                // loga o usuário
                session.setAttribute("usuario", usuario);             
                response.sendRedirect(request.getContextPath() + "/");
                break;
            case "edit":
                usuario = (Usuario)session.getAttribute("usuario");
                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                updateUser(usuario);
                session.setAttribute("notification_type", "success");
                session.setAttribute("notification_message", "Dados atualizados com sucesso!");
                response.sendRedirect(request.getContextPath() + "/user/edituser");
                break;
        }
    }      

    private Usuario addUser(br.ufba.dcc.service.Usuario arg0) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.addUser(arg0);
    }

    private java.util.List<br.ufba.dcc.service.Usuario> listUser() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        return port.listUser();
    }

    private void updateUser(br.ufba.dcc.service.Usuario arg0) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        br.ufba.dcc.service.UsuarioServiceDAO port = service.getUsuarioServiceDAOPort();
        port.updateUser(arg0);
    }
}
