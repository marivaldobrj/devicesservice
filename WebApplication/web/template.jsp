<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <c:set var="baseUrl" value="${pageContext.request.contextPath}" scope="application"/>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>${param.title}</title>
        
        <link rel="stylesheet" href="${applicationScope.baseUrl}/css/jquery-ui.css">
        <link rel="stylesheet" href="${applicationScope.baseUrl}/css/bootstrap.min.css">     
        
        <link rel="stylesheet" href="${applicationScope.baseUrl}/css/application.css">
        
    </head>
    
    <body>            
        <div class="container">
            
            <jsp:include page="/static/navbar.jsp">
                <jsp:param name="page" value="${param.page}" />
            </jsp:include>
            
            <c:if test="${sessionScope.notification_message != null}">
                <jsp:include page="/static/notification.jsp" />
            </c:if>
            
            <div class="jumbotron">
                <c:choose>
                    <c:when test="${param.page != null}">
                        <jsp:include page="${param.page}/${param.content}-content.jsp"/>
                    </c:when>
                    <c:otherwise>
                        <jsp:include page="${param.content}-content.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div><!-- /.container -->        

        <script type="text/javascript" src="${applicationScope.baseUrl}/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="${applicationScope.baseUrl}/js/jquery-ui.js"></script>
        <script type="text/javascript" src="${applicationScope.baseUrl}/js/bootstrap.min.js"></script>
    </body>
</html>