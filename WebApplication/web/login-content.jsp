<%-- 
    Document   : login-content
    Created on : Nov 6, 2014, 11:30:38 PM
    Author     : TestEnv
--%>

<form action="${applicationScope.baseUrl}/login" method="post" role="form">
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Login">
    </div>
    
    <div class="form-group">
        <label for="senha">Senha</label>
        <input type="text" class="form-control" id="senha" name="senha" placeholder="Senha">
    </div>
     
    <button type="submit" class="btn btn-default">Entrar</button>
</form>
