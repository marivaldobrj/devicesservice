<%-- 
    Document   : edituser-content
    Created on : Nov 8, 2014, 11:33:38 PM
    Author     : Marivaldo
--%>

<form action="${applicationScope.baseUrl}/user/edituser" method="post" role="form">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="${sessionScope.usuario.nome}">
    </div>
    
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Login" value="${sessionScope.usuario.login}">
    </div>
    
    <div class="form-group">
        <label for="senha">Senha</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" value="${sessionScope.usuario.senha}">
    </div>
    
    <input type="hidden" name="action" value="edit">
     
    <button type="submit" class="btn btn-default">Salvar</button>
</form>
