<%-- 
    Document   : adddevice-content
    Created on : Nov 1, 2014, 21:10:11 PM
    Author     : Marivaldo
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="p"%>

<div class="panel panel-info">
  <div class="panel-heading">
    Lista de Usu�rios
  </div>


    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Login</th>
                <th>Senha</th>
            </tr>
        </thead>

        <c:forEach var="usuario" items="${requestScope.usuarios}">
            <tbody>
                <tr>
                    <td>${usuario.id}</td>
                    <td>${usuario.nome}</td>
                    <td>${usuario.login}</td>
                    <td>${usuario.senha}</td>
                </tr>
            </tbody>
        </c:forEach>
    </table>
</div>

<%--<p:dateTag id="data" />--%>