<%-- 
    Document   : adduser-content
    Created on : Nov 2, 2014, 11:14:59 PM
    Author     : Marivaldo
--%>

<form action="${applicationScope.baseUrl}/user/listuser" method="post" role="form">
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
    </div>
    
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Login">
    </div>
    
    <div class="form-group">
        <label for="senha">Senha</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
    </div>
    
    <input type="hidden" name="action" value="add">
     
    <button type="submit" class="btn btn-default">Salvar</button>
</form>