<%-- 
    Document   : index-content
    Created on : Nov 2, 2014, 10:14:59 PM
    Author     : Marivaldo
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${sessionScope.usuario != null}">
        <h1>Ol� ${sessionScope.usuario.nome}</h1>
    </c:when>
    <c:otherwise>
        <h1>Web Service</h1>
    </c:otherwise>
</c:choose>
