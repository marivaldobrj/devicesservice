<%-- 
    Document   : listdevices
    Created on : Nov 3, 2014, 1:22:40 AM
    Author     : Marivaldo
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="../template.jsp">
    <c:param name="title" value="Lista de Dispositivos"/>
    <c:param name="content" value="listdevices"/>
    <c:param name="page" value="device"/>
</c:import>