<%-- 
    Document   : listdevice-content
    Created on : Nov 3, 2014, 1:20:35 AM
    Author     : Marivaldo
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags/" prefix="p"%>

<div class="panel panel-info">
    <div class="panel-heading">
         <c:choose>
              <c:when test="${requestScope.usuario == null}">
                  Lista de Dispositivos
              </c:when>
              <c:otherwise>
                  Lista de Dispositivos de ${requestScope.usuario.nome}
              </c:otherwise>
          </c:choose>
    </div>


    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                
                <c:if test="${requestScope.usuario == null}">
                <th>Usu�rio</th>
                </c:if>
            </tr>
        </thead>

        <c:forEach var="dispositivo" items="${requestScope.dispositivos}">
            <tbody>
                <tr>
                    <td>${dispositivo.id}</td>
                    <td>${dispositivo.descricao}</td>
                    <c:if test="${requestScope.usuario == null}">
                        <td><a href="${applicationScope.baseUrl}/device/listdevices?usuario_id=${dispositivo.usuario.id}">${dispositivo.usuario.nome}</a></td>
                    </c:if>
                </tr>
            </tbody>
        </c:forEach>
    </table>
</div>
