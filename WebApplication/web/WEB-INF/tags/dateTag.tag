<%-- 
    Document   : dateTag
    Created on : Oct 14, 2014, 4:16:40 PM
    Author     : chronos
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="id" required="true"%>

<%-- any content can be specified here e.g.: --%>
<input id="${id}" name="${id}" type="text">
<script type="text/javascript">
    $("#${id}").datepicker();
</script>