<div class="alert alert-${sessionScope.notification_type} alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert">
        <span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span>
    </button>
    ${sessionScope.notification_message}
</div>

<%
    try{
        session.setAttribute("notification_message", null);
    }catch(Exception e){
        
    }
%>