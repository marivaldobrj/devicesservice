<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- Static navbar -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Web Service</a>
        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
            
            <ul class="nav navbar-nav">
                <li class="${param.page == 'index' ? 'active' : ''}"><a href="${applicationScope.baseUrl}"><span class="glyphicon glyphicon-home"></span> In�cio</a></li>
                <li class="dropdown ${param.page == 'user' ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Usu�rio <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <c:if test="${sessionScope.usuario == null}">
                            <li><a href="${applicationScope.baseUrl}/user/adduser"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar</a></li>
                        </c:if>
                        <li><a href="${applicationScope.baseUrl}/user/listuser"><span class="glyphicon glyphicon-list"></span> Listar</a></li>
                    </ul>
                </li>                
                <li class="dropdown ${param.page == 'device' ? 'active' : ''}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-phone"></span> Dispositivo <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <c:if test="${sessionScope.usuario != null}">
                            <li><a href="${applicationScope.baseUrl}/device/adddevice"><span class="glyphicon glyphicon-plus-sign"></span> Adidiconar</a></li>
                        </c:if>
                        <li><a href="${applicationScope.baseUrl}/device/listdevices"><span class="glyphicon glyphicon-list"></span> Listar</a></li>
                    </ul>
                </li>                
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
            <c:choose>
                <c:when test="${sessionScope.usuario != null}">   
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">${sessionScope.usuario.nome} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="${applicationScope.baseUrl}/user/edituser"><span class="glyphicon glyphicon-user"></span> Meus Dados</a></li>
                            <li><a href="${applicationScope.baseUrl}/device/listdevices?usuario_id=${sessionScope.usuario.id}"><span class="glyphicon glyphicon-phone"></span> Meus Dispositivos</a></li>
                            <li><a href="${applicationScope.baseUrl}/logout.jsp"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                        </ul>
                    </li>
                </c:when>
                <c:otherwise>
                    <li><a href="${applicationScope.baseUrl}/login"><span class="glyphicon glyphicon-log-in"></span> Entrar</a></li>
                </c:otherwise>
            </c:choose>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>