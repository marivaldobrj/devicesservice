/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufba.dcc.service;

import br.ufba.dcc.dao.DispositivoDAO;
import br.ufba.dcc.dao.UsuarioDAO;
import br.ufba.dcc.model.Dispositivo;
import br.ufba.dcc.model.Usuario;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.WebParam.Mode;
import javax.jws.WebResult;

/**
 *
 * @author Marivaldo
 */
@WebService(serviceName = "UsuarioServiceDAO")
@Stateless()
public class UsuarioServiceDAO {

    /*
    |--------------------------------------------------------------------------
    | USUARIO
    |--------------------------------------------------------------------------
    */
    
    /**
     * This is a sample web service operation
     *
     * @return lista de usuarios
     */
    @WebMethod(operationName = "listUser")
    @WebResult(name = "usuario")
    public List<Usuario> listUser() {

        UsuarioDAO dao = new UsuarioDAO();
        List<Usuario> list = dao.listarUsuarios();

        return list;
    }

    @WebMethod(operationName = "addUser")
    @WebResult(name = "usuario")
    public Usuario addUser(Usuario usuario) {

        UsuarioDAO dao = new UsuarioDAO();
        usuario = dao.cadastrarUsuario(usuario);
        
        return usuario;
    }
    
    @WebMethod(operationName = "getUserById")
    @WebResult(name = "usuario")
    public Usuario getUserById(int usuario_id) {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = dao.getUsuario("id", usuario_id);
        
        return usuario;
    }
    
    @WebMethod(operationName = "updateUser")
    @WebResult(name = "usuario")
    public void updateUser(Usuario usuario) {
        UsuarioDAO dao = new UsuarioDAO();
        dao.atualizarUsuario(usuario);
    }
    
    /**
     * Autentica o usuário
     * 
     * @param login o login do usuário
     * @param senha a senha do usuário
     * @return 
     */
    @WebMethod(operationName = "authorizeUser")
    @WebResult(name = "usuario")
    public Usuario authorizeUser(String login, String senha) {
        
        UsuarioDAO dao = new UsuarioDAO();
        Usuario usuario = dao.autenticarUsuario(login, senha);
        
        return usuario;
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | DISPOSTIVO
    |--------------------------------------------------------------------------
    */
    
    /**
     * This is a sample web service operation
     *
     * @param usuario_id
     * @return lista de dispositivos
     */
    @WebMethod(operationName = "listDevices")
    @WebResult(name = "dispositivo")
    public List<Dispositivo> listDevices(int usuario_id) {
        
        DispositivoDAO dao = new DispositivoDAO();
        List<Dispositivo> list = dao.listarDispositivos(usuario_id);
        
        return list;
    }
    
    @WebMethod(operationName = "listAllDevices")
    @WebResult(name = "dispositivo")
    public List<Dispositivo> listAllDevices() {
        
        DispositivoDAO dao = new DispositivoDAO();
        List<Dispositivo> list = dao.listarDispositivos();
        
        return list;
    }
    
    @WebMethod(operationName = "addDevice")
    @WebResult(name = "dispositivo")
    public Dispositivo addDevice(@WebParam(name = "dispositivo", mode = Mode.IN) Dispositivo dispositivo, int usuarioId) {

        DispositivoDAO dao = new DispositivoDAO();
        System.out.println("UsuarioServiceDAO: "+ dispositivo.getDescricao());
        dao.cadastrarDispositivo(dispositivo, usuarioId);
        
        return dispositivo;
    }
}
