package br.ufba.dcc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.ufba.dcc.jdbc.ConectionFactory;
import br.ufba.dcc.model.Usuario;
import java.sql.Statement;

public class UsuarioDAO {

    private Connection con;

    public UsuarioDAO() {
        con = ConectionFactory.getPostgresConnection();

    }

    public Usuario cadastrarUsuario(Usuario usuario) {
        String sql = "INSERT INTO \"usuarios\" (nome,login,senha) VALUES (?, ?, ?)";
        try {
            PreparedStatement prep = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, usuario.getNome());
            prep.setString(2, usuario.getLogin());
            prep.setString(3, usuario.getSenha());
            System.out.println(prep.toString());
            prep.execute();
            ResultSet rs = prep.getGeneratedKeys();
            if(rs.next())
            {
                usuario.setId(rs.getInt(1));
            }
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usuario;
    }

    /**
     * Atualiza os dados de um usuário
     * 
     * @param usuario 
     */
    public void atualizarUsuario(Usuario usuario) {
        String sql = "UPDATE \"usuarios\" SET nome = ?, login = ?, senha = ? WHERE id = ?";
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setString(1, usuario.getNome());
            prep.setString(2, usuario.getLogin());
            prep.setString(3, usuario.getSenha());
            prep.setInt(4, usuario.getId());
            System.out.println(prep.toString());
            prep.execute();
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletarUsuario(Usuario usuario) {
        String sql = "DELETE FROM \"usuarios\" WHERE nome = ?";
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setString(1, usuario.getNome());
            System.out.println(prep.toString());
            prep.execute();
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Usuario> listarUsuarios() {
        String sql = "SELECT * FROM \"usuarios\"";
        List<Usuario> list = null;
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            list = new ArrayList<Usuario>();
            while (res.next()) {
                Usuario temp = new Usuario();
                temp.setId(res.getInt("id"));
                temp.setNome(res.getString("nome"));
                temp.setLogin(res.getString("login"));
                temp.setSenha(res.getString("senha"));
                list.add(temp);
            }
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    
    public List<Usuario> listarUsuarios(String col, int value) {
        String sql = "SELECT * FROM \"usuarios\" WHERE " + col + " = ?";
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setInt(1, value);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            List<Usuario> list = new ArrayList<Usuario>();
            while (res.next()) {
                Usuario temp = new Usuario();
                temp.setId(res.getInt("id"));
                temp.setNome(res.getString("nome"));
                temp.setLogin(res.getString("login"));
                temp.setSenha(res.getString("senha"));
                list.add(temp);
            }
            prep.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Usuario> listarUsuarios(String col, String value) {
        String sql = "SELECT * FROM \"usuarios\" WHERE " + col + " = ?";
        Usuario temp = null;
        List<Usuario> list = null;
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setString(1, value);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            list = new ArrayList<Usuario>();
            while (res.next()) {
                temp = new Usuario();
                temp.setId(res.getInt("id"));
                temp.setNome(res.getString("nome"));
                temp.setLogin(res.getString("login"));
                temp.setSenha(res.getString("senha"));
                list.add(temp);
            }
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    
    /**
     * Retorna um usuário
     * 
     * @param col
     * @param value
     * @return 
     */
    public Usuario getUsuario(String col, int value) {
        String sql = "SELECT * FROM \"usuarios\" WHERE " + col + " = ? LIMIT 1";
        
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setInt(1, value);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            Usuario usuario = new Usuario();
            while (res.next()) {
                usuario.setId(res.getInt("id"));
                usuario.setNome(res.getString("nome"));
                usuario.setLogin(res.getString("login"));
                usuario.setSenha(res.getString("senha"));                
            }
            prep.close();
            return usuario;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Autenticação do usuário
     * 
     * @param login
     * @param senha
     * @return 
     */
    public Usuario autenticarUsuario(String login, String senha) {
        String sql = "SELECT * FROM \"usuarios\" WHERE login = ? and senha = ?";
        
        Usuario usuario = null;
        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setString(1, login);
            prep.setString(2, senha);
            
            System.out.println(prep.toString());
            
            ResultSet res = prep.executeQuery();
            
            while (res.next()) {
                usuario = new Usuario();
                usuario.setId(res.getInt("id"));
                usuario.setNome(res.getString("nome")); 
                usuario.setLogin(res.getString("login"));
                usuario.setSenha(res.getString("senha"));
            }
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return usuario;
    }

}
