/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufba.dcc.dao;

import br.ufba.dcc.jdbc.ConectionFactory;
import br.ufba.dcc.model.Dispositivo;
import br.ufba.dcc.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marivaldo
 */
public class DispositivoDAO {

    private Connection con;

    public DispositivoDAO() {
        con = ConectionFactory.getPostgresConnection();
    }

    /**
     * Cadastra um dispositivo de um usuário
     * 
     * @param dispositivo
     * @param usuarioId 
     */
    public void cadastrarDispositivo(Dispositivo dispositivo, int usuarioId) {
        String sql = "INSERT INTO \"dispositivos\" (descricao, usuario_id) VALUES (?, ?)";
        try {
            PreparedStatement prep = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, dispositivo.getDescricao());
            prep.setInt(2, usuarioId);
            System.out.println(prep.toString());
            prep.execute();
            prep.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void atualizarDispositivo(Dispositivo dispositivo) {

    }

    public void deletarDispositivo(Dispositivo dispositivo) {

    }

    /**
     * Lista os dispositivos de um usuário
     * 
     * @param usuario_id
     * @return 
     */
    public List<Dispositivo> listarDispositivos(int usuario_id) {

        String sql = ""
                + "SELECT d.id, d.descricao FROM \"dispositivos\" d "
                + "WHERE d.usuario_id = ?";

        try {
            PreparedStatement prep = con.prepareStatement(sql);
            prep.setInt(1, usuario_id);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            List<Dispositivo> list = new ArrayList<Dispositivo>();
            while (res.next()) {
                Dispositivo temp = new Dispositivo();
                temp.setId(res.getInt("id"));
                temp.setDescricao(res.getString("descricao"));
                list.add(temp);
            }
            prep.close();
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Lista todos os dipositivos de todos os usuários
     * 
     * @return 
     */
    public List<Dispositivo> listarDispositivos() {
        String sql = ""
                + "SELECT d.id AS dispositivo_id, d.descricao AS dispositivo_descricao, u.id AS usuario_id, u.nome AS usuario_nome "
                + "FROM \"dispositivos\" d "
                + "INNER JOIN \"usuarios\" u "
                + "ON d.usuario_id = u.id "
                + "ORDER BY u.nome, d.descricao";

        try{
            PreparedStatement prep = con.prepareStatement(sql);
            System.out.println(prep.toString());
            ResultSet res = prep.executeQuery();
            
            
            List<Dispositivo> dispositivos = new ArrayList<Dispositivo>();
            
            while (res.next()) {
                Usuario u = new Usuario();
                Dispositivo d = new Dispositivo();
                
                u.setId(res.getInt("usuario_id"));
                u.setNome(res.getString("usuario_nome"));
                
                d.setId(res.getInt("dispositivo_id"));
                d.setDescricao(res.getString("dispositivo_descricao"));
                d.setUsuario(u);
                
                dispositivos.add(d);
            }
            prep.close();
            return dispositivos;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
