
-- Database: devices_services

-- DROP DATABASE devices_services; ************* COMANDO 1!

CREATE DATABASE devices_services
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;