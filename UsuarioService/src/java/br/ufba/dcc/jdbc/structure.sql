-- Table: "usuarios"

-- DROP TABLE "usuarios"; ************* COMANDO 1!

CREATE TABLE "usuarios"
(
  id serial NOT NULL, -- Identificador do Usuário
  nome character varying(50) NOT NULL,
  login character varying(20) NOT NULL,
  senha character varying(32) NOT NULL,
  CONSTRAINT "usuarios_pkey" PRIMARY KEY (id),
  CONSTRAINT "usuarios_login_key" UNIQUE (login)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "usuarios"
  OWNER TO postgres;
COMMENT ON COLUMN "usuarios".id IS 'Identificador do Usuário';

-- Table: "dispositivos"

-- DROP TABLE "dispositivos"; ************* COMANDO 2!

CREATE TABLE "dispositivos"
(
  id serial NOT NULL, -- Identificador do Dispositivo
  descricao text NOT NULL,
  usuario_id integer NOT NULL,
  CONSTRAINT "dispositivos_pkey" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "dispositivos"
  OWNER TO postgres;
COMMENT ON COLUMN "dispositivos".id IS 'Identificador do Dispositivo';

-- Foreign Key: dispositivos_usuario_id_fkey

-- ALTER TABLE "dispositivos_usuarios" DROP CONSTRAINT dispositivos_usuarios_usuarios_id_fkey;

ALTER TABLE "dispositivos"
  ADD CONSTRAINT "dispositivos_usuario_id_fkey" FOREIGN KEY (usuario_id)
      REFERENCES "usuarios" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE;